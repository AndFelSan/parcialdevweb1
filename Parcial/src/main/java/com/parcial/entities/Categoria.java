package com.parcial.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

@Table(name="CATEGORIA")
@Entity
@NamedQuery(name="Categoria.findAll", query="SELECT c FROM Categoria c")
public class Categoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="ID_CATEGORIA")
	private int id_Categoria;

	@Column(name="NOMBRE")
	private String nombre;

	@OneToMany(mappedBy="categoria")
	private List<Servicio> servicios;

	public Categoria() {
	}

	public int getId_Categoria() {
		return this.id_Categoria;
	}

	public void setId_Categoria(int id_Categoria) {
		this.id_Categoria = id_Categoria;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Servicio> getServicios() {
		return this.servicios;
	}

	public void setServicios(List<Servicio> servicios) {
		this.servicios = servicios;
	}

	public Servicio addServicio(Servicio servicio) {
		getServicios().add(servicio);
		servicio.setCategoria(this);

		return servicio;
	}

	public Servicio removeServicio(Servicio servicio) {
		getServicios().remove(servicio);
		servicio.setCategoria(null);

		return servicio;
	}

}